﻿## Easy3D For HSP3のビルドについて
**ここに書かれている内容は新たにEasy3D For HSP3をフォークする人向けのものです。**  
**Easy3D For HSP3 PlusではREADME.md通りに行えばビルドできます。（これらの作業は必要ありません。）**  

[Easy3D For HSP3](https://github.com/Ochakko/e3dhsp3)はソースコードをダウンロードしただけではビルドできません。  
ビルドするには  

* 開発環境（Microsoft Visual C++ 2010）のインストール
* Microsoft Platform SDKのインストール
* DirectX SDKのインストール
* 各種ライブラリの入手及びビルド
* ソースコードの修正
* プロジェクトファイルの修正

が必要となります。  

本文書では各種インストールは既に終えていることを前提とし、それ以外の工程について述べます。  

## ディレクトリ構成について
この文書で説明に用いる構成を記します。  
必ずしもこのようにしなければならないわけではありません。  
異なったディレクトリ構成を用いる場合は、説明内容を適宜読み替えてください。  

* /e3dhsp3/  
    * おちゃっこ氏のgithubよりダウンロードしたe3dhsp3のソースコードを格納します
* /E3DSamples/
    * おちゃっこ氏のgithubよりダウンロードしたE3dSamplesのソースコードを格納します
* /RokDeBone2/
    * おちゃっこ氏のgithubよりダウンロードしたRokDeBone2のソースコードを格納します
* /libogg/
    * liboggのソースコードを格納します
* /libvorbis/
    * libvorbisのソースコードを格納します
* /zlib/
    * zlibのソースコードを格納します

## 留意点
この構成ではおちゃっこ氏が意図しているであろう構成とは異なった点が幾つか存在します。  
以下に箇条書きで記します。  

* ソリューション、プロジェクトの名称及びそれらとリソースファイルのファイル名「e3dhsp」を「e3dhsp3」に変更
* /e3dhsp3/DX90c/を使用していない
* /e3dhsp3/ZLIB/を使用せず、代わりに/zlib/を使用している

## 各種ライブラリの入手及びビルド
Easy3D For HSP3では以下のライブラリを使用しています。  

* libogg
* libvorbis
* zlib

そのため、これらのライブラリを入手・ビルドしファイルを適切な場所に配置する必要があります。  
後でプロジェクトファイルを書き換えるため配置場所はある程度自由に設定できますが、なるべくわかり易い場所がよいでしょう。  

### libogg
<http://www.xiph.org/downloads/>からダウンロードできます。  
/libogg/に展開したファイル群を配置してください。  
配置したら/libogg/win32/VS2010/libogg_static.slnからDebug・Release両方の設定でビルドしてください。  

### libvorbis
<http://www.xiph.org/downloads/>からダウンロードできます。  
/libvorbis/に展開したファイル群を配置してください。  
配置したら/libvorbis/win32/VS2010/vorbis_static.slnからDebug・Release両方の設定でビルドしてください。  

### zlib
<http://www.zlib.net/>からダウンロードできます。  
/zlib/に展開したファイル群を配置してください。  
配置したら/zlib/contrib/vstudio/vc10/zlibvc.slnからDebug・Release両方の設定でビルドしてください。  

## プロジェクトファイルの修正
いつか書く予定です。  

## ソースコードの修正
いつか書く予定です。  

## 最後に
「プロジェクトファイルの修正」及び「ソースコードの修正」を適用するパッチがpatch.7zとして置いてあります。  
（「各種ライブラリの入手及びビルド」は除く。）  
時間のない方はどうぞ。  