﻿================================================================================

                                     README

               Autodesk FBX SDK 2012.2 release, September 27th, 2011
               -----------------------------------------------------


Welcome to the FBX SDK 2012.2 release! This document includes the latest changes
since the version 2012.1 of the Autodesk FBX SDK.

For more information, please visit us at http://www.autodesk.com/fbx/

Sincerely,
the Autodesk FBX team

================================================================================



TABLE OF CONTENTS
-----------------

    1. New And Deprecated Features
    2. Fixed And Known Issues
    3. Release Notes From Previous Releases
    4. Legal Disclaimer 



1. NEW AND DEPRECATED FEATURES
------------------------------

1.1 New Features

    * Added Area Light support to KFbxLight class.

    * Added Barn Doors support to KFbxLight class.
     
1.2 Deprecated Features

    * HotSpot property on KFbxLight has been changed for InnerAngle.
    
    * ConeAngle property on KFbxLight has been changed for OuterAngle.



2. FIXED AND KNOWN ISSUES
-------------------------

2.1 Fixed Issues

    * Various file I/O crashes.
    
    * Duplicate material issue with material converter is fixed.
    
    * Some minor fixes in sample codes.
    
    * Fixed path issue in Collada reader.
    
    * After writing FBX file, the scene should come back to its initial state.
    
    * Fixed a crash on import of old scenes when it didn't contain blendshapes
      or morpher data.
      
    * Fixed issue with incorrect normals when converting from OBJ to FBX.
    
    * Sample code "Cube Creator" was modifying the scene when exporting in OBJ,
      This has been fixed.
      
    * Fixed an issue with absolute versus relative file paths; now if one of them
      is valid and the other is not, we fix the broken one.
      
    * Fixed an issue with cache writing on Mac when there was a space in the file
      name.
      
    * Fixed an issue with the FBX exporter that would duplicate materials in very
      specific conditions.
      
    * After writing an FBX 6.x file with shapes, the scene was modified and data
      was potentially lost. This has been corrected.
      
    * Fixed an assert caused by stl string in VS2010.
    
    * We now open FBX files in read-only mode rather than read+write. This allow
      for other process to access the FBX file in parallel.
      
2.2 Known Issues

    * File I/O writers might modify the scene permanently upon writing to disk.

    * Deformers do not support instancing. This causes point cache deforming to
      not be re-usable.
      
    * Material layers cannot be instanced.
      
    * Skew and Stretch matrixes are not supported.
    
    * KFbxXMatrix only works with XYZ rotation order.
    
    * The FBX SDK is not multi-thread safe.
    
    * Sometimes, when exporting to various file format, the scene gets modified
      permanently by the writer; data is potentially lost.



3. RELEASE NOTES FROM PREVIOUS RELEASES
---------------------------------------

2012.1

    * Added support for Image Sequences into KFbxVideo.
    
    * Improved the reference documentation quite substantially.

    * Refined how visibility inheritance works: added a new property for each
      node allowing to specify if it should be inherited or not.
      
    * Added a new option in IO Settings to import the time span found in FBX
      files.
      
    * Added a new function to KFbxAxisSystem to retrieve the front vector.
    
    * Added a new function to KFbxAnimEvaluator and KFbxAnimEvalClassic to
      allow users to calculate the local TRS from the global, with an offset.
      See ComputeLocalTRSFromGlobal in kfbxanimevaluator.h for more info.

    * Fix an issue with scale values of zero when passed to the matrix
      converter.

    * Added indentation to various parts of FBX ASCII files.
    
    * Fixed an issue with some curves evaluating to infinite values.
    
    * Multiple blend shapes with the same target resulted in corrupted file,
      this has been corrected.
      
    * An optimization to the file i/o reader and writer improved import
      and export speed up to 60% faster.
      
    * Fixed an issue with constant key mode in the filters.
    
    * Changed the marker look property enum initialization to match the enum
      in the header file.
      
    * Fixed an issue in the classic evaluator which caused channels not
      animated to sometimes return twice the value.

2012.0

    * The FBX SDK Packaging on the Microsoft Windows platform has changed to
      reduce the size of the downloads. Now the different compilers flavors are
      distributed in separate installers. As a result of this, we also
      uniformized the library file names across platforms.

    * The file format version of FBX files as been incremented to 7.2.
      There have been various small changes in the file format to support new
      data.

    * The preprocessor identifier that must be defined when linking with the
      dynamic library version of the FBX SDK has changed from KFBX_DLLINFO to
      FBXSDK_SHARED. KFBX_DLLINFO now serves no purpose. For any other FBX
      SDK configuration no other preprocessor identifiers must be defined.
      
         Note: Please change your projects to define FBXSDK_SHARED if you
         intend to use the dynamic library version of the FBX SDK. 

    * Many improvements to the KFbxCharacter class have been made to improve 
      interoperability of character data with other Autodesk 3D applications.

    * Support for vector displacement maps has been added to the FBX SDK.

    * Support for Allegorithmic's Substance (procedural textures) has been
      added to the FBX SDK core. Now the class KFbxTexture serves as the base
      class for KFbxFileTexture, KFbxLayeredTexture and KFbxProceduralTexture.
      KFbxFileTexture should now be used for bitmap textures, replacing
      KFbxTexture.

    * The Collada importer now supports the Collada 1.4.1 standard.

    * New quaternion evaluation modes for rotation animation curves are now
      available, equivalent to those found in Autodesk Maya.

    * A new type of animation curve key has been added to the FBX SDK core to
      allow for a better support of Autodesk 3dsMax "auto" keys with crease
      in/out.

    * Added support for Dual Quaternion Skinning. Please refer to the new class
      KFbxDualQuaternion for more information.

    * The visibility inheritance behavior of scene nodes have changed to better
      align with popular Autodesk 3D applications.

    * Many improvements to the import time of FBX files have been made.
    
    * The memory footprint of FBX files has been reduced.

    * Support for "in-between" blend shapes has been added in the FBX SDK core
      to allow application plug-ins to import/export these states.

    * Added five new functions to the KFbxNode class :
    
         KFbxXMatrix& KFbxNode::EvaluateGlobalTransform(...)
         KFbxXMatrix& KFbxNode::EvaluateLocalTransform(...)
         KFbxVector4& KFbxNode::EvaluateLocalTranslation(...)
         KFbxVector4& KFbxNode::EvaluateLocalRotation(...)
         KFbxVector4& KFbxNode::EvaluateLocalScaling(...)
      
      These functions are equivalent to calling KFbxScene->
      GetEvaluator()->..., but have been made directly on the FbxNode for
      convenience.
      
    * Added a new function to the KFbxEvaluator abstract class :
    
         void KFbxEvaluator::ResetEvaluationState()
      
      This function enables users to completely clear the whole evaluation
      cache by deleting all entries in it.
      
    * The API documentation generated by Doxygen requires less disk space. Many
      larger images have been removed to make the documentation easier to use.
      
    * Added support for line geometry to FBX via the KFbxLine class.
    
    * Added an option to KMemoryPool to control concurrent allocations.
      Previously, the allocation was always in concurrent mode.
      
    * The FBX SDK is now built with full optimization settings (i.e. both
      size and speed) instead of being optimized for speed only.
      
    * Roughly 25 new texture blend modes were added. Also, the default blend
      mode is now Normal instead of Translucent.
      
    * Object names will now appear in ASCII FBX files as comments in the
      connection section for easier debugging.
      
    * A new tessellation function was added to NURB curves to provide a work-
      around for applications that do not support them. The tessellation will
      produce a KFbxLine object (a series of vertices).
      
    * A new file, fbxfilesdk_version.h has been added that contains all the
      versioning information of the FBX SDK. It also defines a new preprocessor
      identifier FBXSDK_VERSION_STRING that represents the version information
      as a string.
      
    * Ptex files can be transported just like any other texture format (png,
      jpeg, etc.) provided that they are connected to a material property. For
      more information about Ptex please visit http://ptex.us/documentation.html
      
    * A new function KFbxImporter::SetEmbeddingExtractionFolder as been added
      to allow developers to specify an extract folder when importing FBX files.
      
    * New functions added to KFbxCache class: GetNextTimeWithData(),
      GetDataCount() and GetDataTime().
      
    * All FBX related DLLs shipped are now officially signed with the Autodesk
      certificate.
     
    * The two KFbxSdkManager::CreateClass(...) functions have been marked
      deprecated and KFbxSdkManager::CreateNewObjectFromClassId(...) should be
      used instead.
      
    * The methods KFbxNode::Set/GetUseQuaternionForInterpolation() have been
      marked deprecated. Their usage should be replaced with calls to
      KFbxNode::Set/GetQuaternionInterpolation() respectively. These new
      methods are using the following enumeration symbols to activate new
      evaluation algorithms:

         eQUATINTERP_OFF = 0
         eQUATINTERP_CLASSIC = 1
         eQUATINTERP_SLERP
         eQUATINTERP_CUBIC
         eQUATINTERP_TANGENTDEPENDENT

      The eQUATINTERP_OFF is equivalent to the old way of passing the "false"
      value to the parameter of the SetUseQuaternionForInterpolation() while
      eQUATINTERP_CLASSIC correspond to the "true" value. The classic mode call
      the evaluation algorithm that has been defined in the FBX SDK from the
      earlier versions.
      
    * The methods KFbxSdkManager::CreateClass have been deprecated in favor for
      a new function, KFbxSdkManager::CreateNewObjectFromClassId(...) which
      removes non-used parameters, and combine all functions into this single
      call. The new function name also clearly indicate that a new object
      instance is created, rather than a new class.
      
    * The methods to access the properties of KFbxSurfaceMaterial and sub-class
      have been deprecated. Now users can simply access the properties directly
      just like any other FBX SDK object class.
      
    * Many functions of KFbxShape have been deprecated with the introduction
      of the support for in-between blendshapes.
      
    * The .3DS file format writing as been retired. However, the .3DS file
      reader will persist.

    * The mDEBUG_String member was removed from the KString class to make sure
      the debug and release version are the same size. As a result of this, to
      debug the KString class on the Microsoft Windows platform, please open
      and read the file kstring.txt provided along this readme.txt file.

    * The static libraries are now built with _SECURE_SCL=1, which is the
      default value when not specified by projects. If your project used to
      define _SECURE_SCL=0 just to be able to link with the FBX SDK, then it
      can now be safely removed from the predefined preprocessor identifiers.

    * KFbxObject::Clone has been reviewed, and now offers an implementation
      that should fit most, if not all, cloning logic for all classes that
      inherit from KFbxObject. As a result objects that don't implement Clone
      behave correctly and similarly across the SDK.
      
    * Fixed an issue in the conversion of Euler value to quaternion values with 
      some corner cases.
      
    * The FBX SDK no longer requires RTTI to be enabled by the host software.
      This also means dynamic_cast can no longer be used to convert FBX SDK
      objects. KFbxCast<CLASSTYPE> should be used instead.
      
    * The class KFbxSurfacePhong can now be converted into its parent class,
      KFbxSurfaceLambert, using KFbxCast.

    * The properties of KFbxSurfaceMaterial and all its sub-classes are now
      declared public.
      
    * The function KFbxObject::GetTypeName() has been re-vamped to return a
      const char* type instead of KString. Many classes that were
      re-implementing this function without reason have been cleaned-up
      throughout the SDK.
      
    * The function ResetPivotSetAndConvertAnimation will now also iterate
      through all animation stacks instead of just the first one it finds in
      the scene.
      
    * Fixed a crash wih KFbxAnimEvaluator::SetContext(...).
    
    * A pragma pack directive was added to the file fbxsdk.h to set it to the
      value used when the FBX SDK was compiled. This will allow developers
      to include the FBX SDK in their project without having to worry about the
      packing size used by their application.
      
    * KFbxCache::GetChannelIndex() should return -1 instead of 0 when error
      occurs or channel not found.
      
    * Fixed several issues and memory leaks in FBX SDK sample codes.
    
    * KString size in debug and release should now be the same, which fix the
      crash resulting of that incompatibility.
      
    * The Microsoft Visual Studio 2008 builds are now compiled using the
      Service Pack 1.
      
    * Added the const qualifier to several functions and members.
    
    * Stereo Cameras now have the correct post-rotation set on export.
    
    * An issue with multiple property-to-property connections in FBX files has
      been corrected.
      
    * An optimization to the way we allocate animation curves has been done to
      improve the destroy speed with large scenes.
      
    * Fixed an issue in KFbxMesh::TriangulateMeshAdvance(), an incorrect mesh
      smooth group was set when the original mapping mode was "by edge".
      
    * An issue with skew matrix in the classic evaluator has been fixed.

2011.3.1

    * All functions related to local and global "state" in the KFbxNode
      declaration were removed since their implementation didn't work anymore
      since the last release. They should have been set to deprecated within
      the header files, but they were forgotten.
      
    * The unroll filter was inconsistent when applying its process on curves
      data, this has been corrected.

    * Fixed a crash in the function EmulateNormalsByPolygonVertex.
    
    * Visual Studio 2010 builds were reporting missing PDB files, this has been
      corrected.
      
    * Importing various .OBJ file sometime resulted in much more vertices, this
      is now working as intended.
      
    * The mesh triangulation method has been re-written for a much more robust
      algorithm. Now it can handle non-concave shapes. The method can be called
      using TriangulateMeshAdvanced.
      
    * A conversion error from Euler to Quaternion has been corrected, it is now
      much more robust for corner cases.

2011.3

    * The FBX SDK is now also ship in a dynamic library package format. To link
      against the dynamic version, users will have to define KFBX_DLLINFO in
      their project preprocessor settings. The static version still doesn't
      require any preprocessor to be defined.

    * Augmented the KFbxCache class to correctly support multiple data channels.

    * Stereo Cameras now correctly support aim/up.
    
    * Added three new functions to the KFbxAnimEvaluator class :
    
      KFbxVector4& GetNodeLocalTranslation(...)
      KFbxVector4& GetNodeLocalRotation(...)
      KFbxVector4& GetNodeLocalScaling(...)
      
      Allow users to calculate the local translation, rotation and scaling as it
      was done in previous versions of the FBX SDK. On purpose, these new
      functions will not take pre/post rotations, offsets and pivots in
      consideration, but they still will consider translation, rotation and
      scaling limits.

    * Added the new KFbxCachedEffect node attribute. These are used to store
      other kind of vertex (or position) caching.

    * Fixed a crash at import time for various legacy FBX files when importing
      animation curves data.

    * Some UV Sets were lost in very specific cases during export, this has
      been corrected.

    * Fixed an issue with node's local transform calculation. Now it should
      correctly return the result of ParentGlobal.Inverse * Global.

    * Protein 2.0 Materials are now extract in the .fbm folder along the .fbx
      file first, rather than the user's operating system temporary folder.
      
    * The following files contain many newly deprecated calls. Please open them
      and search for the macro K_DEPRECATED to find out. Because the list is so
      big, it will not be listed here.
      
      kfbxconstraint.h, kfbxconstraintaim.h, kfbxconstraintparent.h,
      kfbxgeometry.h, kfbxnode.h, kfbxscene.h, kfbxkfcurvefilters.h,
      kfbxdocument.h, kfbxreaderfbx.h, kfbxreaderfbx6.h, kfbximporter.h,
      kfbxproperty.h

2011.2

    * Officially dropped support for PowerPC architecture. Universal binaries
      found in MacOS builds will now only contain 32/64-bit variances.

    * Fixed a crash when importing some legacy MotionBuilder FBX 5.x files.
    
    * Corrected the computation of the smoothing group for in mesh triangulation
      function.
    
    * Fixed Localization problems on the DAE, DFX and OBJ readers/writers.
    
    * Extended the file size limit for FBX files from 2GB to 4GB.
    
    * Augmented the Reference Documentation for a certain number of classes. For
      example, check out KFbxNode or KFbxObject and tell us what you think! :)

2011.1

    * Removed the KFbxTakeNodeContainer class. This was done with the redesign
      of the animation system.

    * A whole new set of classes are now available to evaluate animation with
      the FBX SDK. For more information, please look at the reference
      documentation for KFbxAnimStack, KFbxAnimLayer, KFbxAnimCurve,
      KFbxAnimCurveNode, KFbxAnimEvaluator and KFbxAnimEvalClassic classes. Also
      the evaluation result will now be stored outside the KFbxNode class and
      only created on demand, resulting in a much smaller memory footprint.

    * Removed all needed preprocessor defines to be able to correctly link with
      the static version of the FBX SDK. Namely, those defines were K_PLUGIN,
      K_FBXSDK and K_NODLL.
      
    * The FBX file format as now been upgraded to our latest new technology,
      FBX 7.1! This new FBX file format allow for much more flexibility,
      supporting any number of instances, connections by GUID, reduced file size
      with compression, embedding in ASCII files and much more!
      
    * The KFbxSystemUnit class changed so that it doesn't modify the multiplier
      parameter. Now it is simply carried along in the FBX file.
      
    * A Python Binding for FBX SDK has been released for the first time! In this
      first release, only the most basic functions to allow import/export and
      scene traversal and property query has been exposed. More will be exposed
      later on when we gather more feedback from user experience.
      
2010.2

    * Improved processing speed of the function to retrieve polygon's indexes in
      the mesh class KFbxMesh.
      
    * Removed SetFileFormat on all classes that inherit from KFbxImporter and
      KFbxExporter. Instead, the file format can be overwritten in the
      Initialize functions. By default, the file format will now be auto-
      matically be detected.
      
    * Extended the KFbxMesh class to support standard mesh smoothing. We are
      referring to edge/vertex creases, mesh smoothness, division levels,
      subdivisions, continuity and border/edge preservation.

    * Added Stereo Cameras support via the KFbxCameraStereo class.

    * Added Display Layer and Selection Sets support via the KFbxDisplayLayer
      and KFbxSelectionSet classes respectively.

    * Fixed an issue preventing the use of the slash ( / ) character in property
      names.
      
    * Fixed a stack overflow error in KFbxRenamingStrategy.
    
    * Added support for many new texture blend mode. See KFbxLayerElementTexture
      for more information.

    * Files not ending in .fbx but that still contain FBX formatting can now
      successfully be opened with the FBX SDK.
      
    * Properties can now be destroyed with KFbxProperty::Destroy().

    * Fixed FBX 5.x reader to correctly set pivot information when importing
      legacy files.
      
2010.0

    * Dropped support for Microsoft Visual Studio 2003 libraries.
    
    * Many, many issues fixed. Please refer to previous readme versions for more
      details.
      
2009.x

    * KFbxCache class supports int array.
    
    * Added the Subdivision Display Smoothness to the KFbxSubdiv class.
    
    * Added the optional argument to the IsValid() method in the KFbxTrimSurface
      class to skip the check of boundary curves CV's overlaps.
      
    * Re-factoring of the KFbxCamera class.
    
    * Updates to class documentation.

    * Added methods and properties to manipulate front/back planes & plates.
    
    * Deprecated ECameraBackgroundDrawingMode type and replaced with
      ECameraPlateDrawingMode
    
    * Deprecated ECameraBackgroundPlacementOptions. This has been replaced with
      the individual properties: FitImage, Center, KeepRatio and Crop.
    
    * Deprecated GetBackgroundPlacementOptions() since now the flags are stored
      in the above mentioned properties.
    
    * Deprecated SetViewFrustum(bool pEnable), use SetViewNearFarPlanes()
    
    * Deprecated GetViewFrustum(), use GetViewNearFarPlanes()
    
    * Support of non-convex polygons in the triangulation algorithms.
    
    * Overload of the new operator is not possible anymore (the
      FBXSDK_OVERLOAD_NEW_OPERATOR macro has been removed). The usage of the
      custom memory allocator can only be achieve by using the
      KFbxMemoryAllocator class. See the ExportScene05 for an implementation
      example.
    
    * Enhanced algorithm for smoothing groups generation.
    
    * Support of displacement map channel.
    
    * The class KFbxStreamOptions is now obsolete and is gradually being
      replaced by the class KFbxIOSettings.
    
    * Added KFbxConstraintCustom class.
    
    * Added KFbxContainerTemplate class.
    
    * Added KFbxSubdiv class.
    
    * Added KFbxEmbeddedFilesAccumulator class.
    
    * Adjusted tangents to stay closer to the real value when the weight gets
      ridiculously small.
    
    * Fixed Collada plug-in to handle operating system locale. Depending on the
      locale, the decimal point for numbers may have been represented with the
      comma instead of the point causing parsing errors.
      
    * Fixed support for the floor contact to the KFbxCharacter.
    
    * Fixed infinite loop when loading .obj files on MAC OS.
    
    * Removed some more memory leaks.
    
    * Added the HasDefaultValue(KFbxProperty&) function to check if a property
      value has changed from its default one.
    
    * Added the BumpFactor property to the SurfaceMaterial class.
    
    * Defined plug-ins of plug-ins interface in the fbxsdk manager. See the
      Autodesk FBX SDK PRW readme file for more details.
    
    * Re-factoring of the renaming strategy object.
    
    * Removed unused eCONSTRAINT from the KFbxNodeAttribute.
    
    * Deprecated FillNodeArray and FillNodeArrayRecursive
    
    * Overwrite empty relative filename in texture objects with the correct
      value.
    
    * Fix for internal TRS cache so it correctly get reset when changing takes.
    
    * Bug fixes in the Collada reader/writer.
    
    * Fixed a bug that was causing the loss of Shape animation on NURBS objects.
    
    * Fixed the fact that the layers were losing their name after a clone.
    
    * Corrections for pivot conversion functions:
    
      - Set source pivot to ACTIVE in function ResetPivotSetAndConvertAnimation.
      
      - Update default transformation values to match the results of the pivot
        conversion functions.
      
    * Fixed the endless loop in the RemoveChar() method of the KString class.
    
    * Fixed default values in the KFbxCharacter structure.



4. LEGAL DISCLAIMER
-------------------

Autodesk and FBX are registered trademarks or trademarks of Autodesk, Inc., in
the USA and/or other countries. All other brand names, product names, or trade-
marks belong to their respective holders.

                  (C) 2011 Autodesk, Inc. All Rights Reserved.

================================================================================
