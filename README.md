﻿#Easy3D For HSP3 Plus

[Easy3D For HSP3](https://github.com/Ochakko/e3dhsp3)をフォークし、バグフィックスや機能追加を図った非公式なライブラリです。  
Easy3D For HSP3は開発が中止されてしまいましたが、長きにわたって用いられてきたこともあり現在もなお使用されています。  
既存のプロジェクトで用いられているEasy3D For HSP3を別のライブラリに置き換えることは多大な労力を伴い、また思わぬバグの発生源ともなります。  
そこで、せめてライブラリの保守点検くらいは出来ないだろうかと考え、開発を始めました。  

## 使い方
基本的にはEasy3D For HSP3と変わりません。  
互換性も極力残すようにしています。  

## ビルド方法
1. 以下のものをインストールします
    * 開発環境（Microsoft Visual C++ 2010）
    * Microsoft Platform SDK
    * DirectX SDK
2. 以下のものをDebug・Release両方の設定でビルドします
    * /libogg/win32/VS2010/libogg_static.sln
    * /libvorbis/win32/VS2010/vorbis_static.sln
    * /zlib/contrib/vstudio/vc10/zlibvc.sln
3. /e3dhsp3/e3dhsp3.slnをビルドします
4. /e3dhsp3/Debug/もしくは/e3dhsp3/Release/にファイルが出力されます

## Easy3D For HSP3との相違点
以下の点がEasy3D For HSP3と異なります。

* 使用ライブラリのバージョン
    * libogg（1.2.2→1.3.2）
    * libvorbis（1.3.2→1.3.4 aoTuV）
    * zlib（？→1.2.8）

## ライセンス
OSS版のEasy3D For HSP3のライセンスはLGPLとなっています。  
ですので、こちらのライブラリもLPGLとします。  

## 免責事項
このプロジェクトは非公式なものです。  
ですので、このライブラリを使用して発生した問題についてはおちゃっこ氏には問い合わせないでください。  
また、個人で開発を行っているため充実したサポートなどは受けられない可能性が高いです。  
ライブラリの使用に関しても自己責任とします。  
以上を了承できる方のみご使用願います。  

## 作者
Easy3D For HSP3の製作者はおちゃっこ氏です。  
Easy3D For HSP3 Plusの開発は現在Flatが行っています。  